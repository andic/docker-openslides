ARG ARG_NAME=openslides
ARG ARG_VERSION=3.3
ARG ARG_USER_NAME=${ARG_NAME}
ARG ARG_GROUP_NAME=${ARG_NAME}


## Build Container
FROM alpine:3.14 as builder

ARG ARG_NAME
ARG ARG_VERSION
ENV NAME=${ARG_NAME}
ENV VERSION=${ARG_VERSION}

RUN set -ex \
  && echo "Building ${NAME} - ${VERSION}" \
  && apk update \
  && apk upgrade \
  && apk add \
    build-base \
    libffi-dev \
    py3-pip \
    python3 \
    python3-dev \
  && rm -rf /var/cache/apk/* \
  && pip install --upgrade pip \
  && pip install wheel \
  && pip install --root /tmp/${NAME} openslides==${VERSION} --no-warn-script-location \
  && echo "Done building ${NAME} - ${VERSION}"


## Runtime Container
FROM alpine:3.14

LABEL org.opencontainers.image.authors="Andreas Cislik <git@a11k.net>"
LABEL org.opencontainers.image.source="https://gitlab.com/andic/docker-openslides"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.title="OpenSlides"
LABEL org.opencontainers.image.description="The digital motion and assembly system"

ARG ARG_NAME
ARG ARG_VERSION
ARG ARG_USER_NAME
ARG ARG_GROUP_NAME
ENV NAME=${ARG_NAME}
ENV VERSION=${ARG_VERSION}
ENV USER_NAME=${ARG_USER_NAME}
ENV GROUP_NAME=${ARG_GROUP_NAME}

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
VOLUME /opt/${NAME}/data
EXPOSE 8000

#WORKDIR /opt/${NAME}
COPY --from=builder /tmp/${NAME}/usr /usr
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh

RUN set -ex \
  && echo "Installing ${NAME} - ${VERSION}" \
  && apk update \
  && apk upgrade \
  && apk add \
    gettext \
    python3 \
    py3-pip \
    tzdata \
  && rm -rf /var/cache/apk/* \
  && chmod 755 /usr/local/bin/docker-entrypoint.sh \
  && addgroup ${GROUP_NAME} -g 1000 \
  #&& adduser -u 1000 -G ${GROUP_NAME} --home /opt/${NAME} -D ${USER_NAME} \
  && adduser -u 1000 -G ${GROUP_NAME} -D ${USER_NAME} \
  #&& useradd -r -m -d /opt/${NAME} ${USER_NAME} \
  #&& mkdir -p /etc/${NAME} \
  && echo "Done installing ${NAME} - ${VERSION}" \
