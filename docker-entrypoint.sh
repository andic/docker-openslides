#!/bin/sh

echo "starting docker-entrypoint.sh"
echo "$(date)"
echo "${NAME} - ${VERSION}"


## create config
#if [ ! -e /opt/galene/data/ice-servers.json ]; then
#  cp /opt/galene/templates/data/passwd /opt/galene/data/passwd
#  cp /opt/galene/templates/data/ice-servers.json /opt/galene/data/ice-servers.json
#  cp /opt/galene/templates/groups/public.json /opt/galene/groups/public.json
#fi


## start service
#cd ~${USER_NAME}
#chown -R ${USER_NAME}:${GROUP_NAME} data
chown -R ${USER_NAME}:${GROUP_NAME} /home/${USER_NAME}
su ${USER_NAME} -c "openslides $@"
#su ${USER_NAME} -c "export PATH=${PATH}:/usr/local/bin && export PYTHONPATH=${PYTHONPATH}:/usr/local/lib/python3.10/site-packages && python3 /usr/local/bin/openslides $@"


echo "stopping docker-entrypoint.sh"
echo "$(date)"
